# LINUX ONLY

# mkdesk

a program/command to create .desktop files (program launchers) using the terminal

# mkd

a more minimal version of mkdesk

## how to use: mkd "name of program" "command to be executed"
    example: mkd Reddit firefox https://reddit.com

# INSTALATION
## clone this repository
    git clone https://gitlab.com/mr-draxs/mkdesk.git
## run ./install as sudo
    chmod +x ./install
    sudo ./install
