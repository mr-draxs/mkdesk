#!/bin/bash

box -l What would you like to name your application.desktop?
read -p "Application name: " name

box -l What would you like to launch?
read -p "Exec: " app

box -l [Desktop Entry] -l Type=Application -l Name=$name -l Exec=$app

read -e -p "Do you like to save $name.desktop? " choice

if [[ $choice == [Yy]* ]]; then
	if [[ ! -d $HOME/.local/share/applications ]]; then
		mkdir $HOME/.local/share/applications
box "$HOME/.local/share/applications folder was created"
	fi
	echo "[Desktop Entry]
	Type=Application
	Name=$name
	Exec=$app" > "$HOME/.local/share/applications/$name.desktop"
box "$name.desktop saved in $HOME/.local/share/applications"
else
box "[Operation Canceled]"
fi

