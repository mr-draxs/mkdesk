#!/bin/bash

if [ "$1" == "-h" ]; then

box "to use mkd you first have to digit mkd then the name of you application.desktop and then what commnad/program-directory you want to launch"

else

if [ -n "$1" ]; then

app=($@)
unset app[0]

box -l [Desktop Entry] -l Type=Application -l Name=$1 -l Exec=${app[@]} 

echo "[Desktop Entry]
Type=Application
Name=$1
Exec=${app[@]}" > "$HOME/.local/share/applications/$1.desktop"

box "$name.desktop saved in $HOME/.local/share/applications"

else
box "mkd [name] [command/-dir]"
fi
fi
