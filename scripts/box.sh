#!/bin/bash

words=($@)
lines=()
lc=0
l=0
bl=0

for ((i = 0; i < ${#words[@]}; i++))
do
  li="$(( $l + $i ))"
      lines[$lc]="${lines[$lc]} ${words[$li]}"
      wc=$(( wc + 1 ))
  if [[ $wc = 10 ]]; then
    lc=$(( $lc + 1 ))
    l=$(( ${#lines[$i]} * 5 ))
    wc=0
  fi
done

for w in ${words[@]} 
do
  if [[ $w == "-l" ]]; then
	lines[nol]=$(echo "$@" | awk -F '-l' -v i=$nol '{print $(i+2)}')
	if [[ ${lines[nol]} == '' ]];then unset lines[nol]; fi
	nol=$(( $nol + 1 ));
  fi
done

for ((l = 0 ; l < ${#lines[@]}; l++))
do
  if (( $bl < ${#lines[$l]} )); then
    bl=${#lines[$l]}
  fi
done

for ((i = 0 ; i < $bl + 1; i++))
do
  mb=$mb―
done

echo "╭$mb╮"
for ((i = 0 ; i < ${#lines[@]} ; i++))
do
lnwsp=${#lines[$i]}
bwsp=$((bl-lnwsp))
  for ((ni = 0 ; ni < $bwsp; ni++))
  do
    wsp=$wsp-
  done
echo "│${lines[$i]}${wsp//-/ } │"
wsp=""
done
echo "╰$mb╯"

